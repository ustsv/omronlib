#-------------------------------------------------
#
# Project created by QtCreator 2016-02-08T13:42:02
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets network

TARGET = OmronQtGIU
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../lib/ -lplcQlib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../lib/ -lplcQlibd
else:unix: LIBS += -L$$PWD/../../lib/ -lplcQlib

INCLUDEPATH += $$PWD/../../plcQlib
DEPENDPATH += $$PWD/../../plcQlib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../lib/libplcQlib.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../lib/libplcQlibd.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../lib/plcQlib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../lib/plcQlibd.lib
