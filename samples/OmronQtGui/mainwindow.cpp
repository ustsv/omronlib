#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QCoreApplication>
#include <QFileInfo>

#define VERSION "0.9.0"
#define PLC_IP_1 "192.168.1.10"
#define NOT_FOR_PRODUCTION 1

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //Init pointers
    plc1Proxy = NULL;
    translator = NULL;

    disable_shutdown = false;

    //Connect to plc after gui is up and running
    QTimer::singleShot(10, this, SLOT(initMainWindow()));
    timerPer1000 = new QTimer();

    //restart timer when comm fail event
    plc1RestartTimer = new QTimer();
    plc1RestartTimer->setInterval(3000);
    QObject::connect(plc1RestartTimer, SIGNAL(timeout()), this, SLOT(plc1Restart()));
}

MainWindow::~MainWindow()
{
    plc1Proxy->stopProxy();
    delete plc1Proxy;
    plc1Proxy = NULL;

    delete ui;
}

void MainWindow::initMainWindow()
{
    //GUI itself

    plc1Proxy = new PlcProxy(PLC_IP_1);

    //register for read errors from proxy
    QObject::connect(plc1Proxy, SIGNAL(error(PlcProxy::EplcProxyError)), this, SLOT(plc1ProxyError(PlcProxy::EplcProxyError)));

    initPlc1WidgetsStructure(); //setup notifiers
    startPlc1Comm();

    //periodic called functions (state machine ..., plc alive)
    timerPer1000->start(1000);
    QObject::connect(timerPer1000, SIGNAL(timeout()), this, SLOT(periodic1000ms()));

    //main tab page is changed
    QObject::connect(ui->mainTabWgt, SIGNAL(currentChanged(int)), this, SLOT(mainTabChanged(int)));

    checkFullScreen();
}

void MainWindow::startPlc1Comm()
{
    if (plc1Proxy) {
        plc1Proxy->flushRegions(); //delete regions registrations
        plc1Proxy->disableWrite();

        //Main PLC
        //        plc1Proxy->addRegion("H0",100,200);
        //        plc1Proxy->addRegion("H100",100,200);
        plc1Proxy->addRegion("C4030", 100, 1000);
        plc1Proxy->addRegion("D1200", 100, 1000);
        plc1Proxy->addRegion("D1800", 100, 1000);
        plc1Proxy->addRegion("D1500", 100, 1000);
        plc1Proxy->addRegion("D910", 2, 1000);
        //       plc1Proxy->addRegion("H4023", 2, 1000);

        //read all values from PLC
        plc1Proxy->startProxy(true); //force notify - init whole gui
        plc1Proxy->stopProxy();
        plc1Proxy->startProxy();
        plc1Proxy->enableWrite();

    } else {
        qCritical() << "PLC proxy object not found ..";
    }
}

void MainWindow::initPlc1WidgetsStructure()
{
    qDebug() << "MainWindow::initPlcWidgetsStructure";
    plc1EventHandler.Init(plc1Proxy); //inputs

    widget1EventHandler.Init(plc1Proxy); //outputs

    PlcEvent *pa;

    PlcWidgetSender *pws;

    QString trueStyle = "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #80db80, stop: 1 #90fb90);";
    QString falseStyle = "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #db8080, stop: 1 #fb8080);";

    //HMI - indikators - change vissibility from PLC:
    //    plc1EventHandler.Add("C4023:02", new PlcEvent_QWidget(PlcEvent::VISIBLE_IF_TRUE, ui->l_h10_1_s1));

    //string formater for labels
    QTextStream *sf = new QTextStream(new QString());
    sf->setRealNumberPrecision(1);
    sf->setRealNumberNotation(QTextStream::FixedNotation);

    QTextStream *sr = new QTextStream(new QString());
    sr->setRealNumberPrecision(0);
    sr->setRealNumberNotation(QTextStream::FixedNotation);

    //HMI - indikators - numbers form PLC to labels:
    //    plc1EventHandler.Add( "H20", new PlcEvent_QLabel( PlcEvent::UPDATE_VALUE_FLOAT, ui->l_h20f_s3, sf, 2));
    //    plc1EventHandler.Add( "H19", new PlcEvent_QLabel( PlcEvent::UPDATE_VALUE_UNSIG, ui->l_h19w_s2, sr, 1));
    //    plc1EventHandler.Add( "H44", new PlcEvent_QLabel( PlcEvent::UPDATE_VALUE_BCD, ui->l_h44w_sd, sr, 1));

    //HMI - controls - spinbox
    plc1EventHandler.Add("D910", new PlcEvent_QSpinBox(PlcEvent::UPDATE_VALUE_SIG, ui->D910, 2)); //readback from plc
    //    widget1EventHandler.Add("D910", new SpinboxPlcSender(ui->D1822, "L1_W", ui->D1822), PlcProxy::TYPE_UWORD_2W);

    //car
    plc1EventHandler.Add("C4030", new PlcEvent_QSpinBox(PlcEvent::UPDATE_VALUE_UNSIG, ui->D4030, 2)); //readback from plc
    plc1EventHandler.Add("C4012", new PlcEvent_QSpinBox(PlcEvent::UPDATE_VALUE_UNSIG, ui->D4030, 2)); //readback from plc
    //    widget1EventHandler.Add("C4030", new SpinboxPlcSender(ui->D4030, "L1_Sno", ui->D4030), PlcProxy::TYPE_SWORD_2W);

    pws = new PlcWidgetSender("C4023:02", ui->repeat, plc1Proxy);
    pws->IOPressButton();

    pws = new PlcWidgetSender("C4012:04", ui->carLeft, plc1Proxy);
    pws->IOPressButton();

    pws = new PlcWidgetSender("C4012:05", ui->carRight, plc1Proxy);
    pws->IOPressButton();

    plc1EventHandler.Add("D1820", new PlcEvent_QDoubleSpinBox(PlcEvent::UPDATE_VALUE_FLOAT, ui->D1820, 2)); //readback from plc
    widget1EventHandler.Add("D1820", new SpinboxPlcSender(ui->D1820, "L1_W", ui->D1820), PlcProxy::TYPE_FLOAT_2W);

    plc1EventHandler.Add("D1822", new PlcEvent_QDoubleSpinBox(PlcEvent::UPDATE_VALUE_FLOAT, ui->D1822, 2)); //readback from plc
    widget1EventHandler.Add("D1822", new SpinboxPlcSender(ui->D1822, "L1_W", ui->D1822), PlcProxy::TYPE_FLOAT_2W);

    plc1EventHandler.Add("D1566", new PlcEvent_QDoubleSpinBox(PlcEvent::UPDATE_VALUE_FLOAT, ui->D1566, 2)); //readback from plc
    widget1EventHandler.Add("D1566", new SpinboxPlcSender(ui->D1566, "L1_W", ui->D1566), PlcProxy::TYPE_FLOAT_2W);

    plc1EventHandler.Add("D1504", new PlcEvent_QDoubleSpinBox(PlcEvent::UPDATE_VALUE_FLOAT, ui->D1504, 2)); //readback from plc
    //    widget1EventHandler.Add("D1504", new SpinboxPlcSender(ui->D1504, "L1_W", ui->D1504), PlcProxy::TYPE_FLOAT_2W);

    // rot
    plc1EventHandler.Add("D1204", new PlcEvent_QDoubleSpinBox(PlcEvent::UPDATE_VALUE_FLOAT, ui->D1204, 2)); //readback from plc
    widget1EventHandler.Add("D1204", new SpinboxPlcSender(ui->D1204, "L1_W", ui->D1204), PlcProxy::TYPE_FLOAT_2W);

    plc1EventHandler.Add("D1220", new PlcEvent_QDoubleSpinBox(PlcEvent::UPDATE_VALUE_FLOAT, ui->D1220, 2)); //readback from plc
    widget1EventHandler.Add("D1220", new SpinboxPlcSender(ui->D1220, "L1_W", ui->D1220), PlcProxy::TYPE_FLOAT_2W);

    plc1EventHandler.Add("D1208", new PlcEvent_QDoubleSpinBox(PlcEvent::UPDATE_VALUE_FLOAT, ui->D1208, 2)); //readback from plc
    //    widget1EventHandler.Add("D1208", new SpinboxPlcSender(ui->D1208, "L1_W", ui->D1208), PlcProxy::TYPE_FLOAT_2W);

    plc1EventHandler.Add("D1234", new PlcEvent_QDoubleSpinBox(PlcEvent::UPDATE_VALUE_FLOAT, ui->D1234, 2)); //readback from plc
    widget1EventHandler.Add("D1234", new SpinboxPlcSender(ui->D1234, "L1_W", ui->D1234), PlcProxy::TYPE_FLOAT_2W);

    pws = new PlcWidgetSender("C4030:12", ui->rotUp, plc1Proxy);
    pws->IOPressButton();

    pws = new PlcWidgetSender("C4030:13", ui->rotDn, plc1Proxy);
    pws->IOPressButton();

    pws = new PlcWidgetSender("C4031:00", ui->CW, plc1Proxy);
    pws->PressToInvert();

    pws = new PlcWidgetSender("C4031:01", ui->CCW, plc1Proxy);
    pws->PressToInvert();

    //Finnaly:
    //register readback signal from Proxy to this object
    QObject::connect(this->plc1Proxy, SIGNAL(readed(QString, QList<u16>)), &plc1EventHandler, SLOT(plcEventService(QString, QList<u16>)));
}

void MainWindow::plc1ProxyError(PlcProxy::EplcProxyError error)
{
    qDebug() << "PLC ERROR" << error;

    if (!plc1RestartTimer->isActive()) {
        qDebug() << " ...Start Timer";
        plc1RestartTimer->start(3000);
    }
}

void MainWindow::plc1Restart()
{
    qDebug() << "PLC RESTART";
    plc1RestartTimer->stop();
    plc1Proxy->stopProxy();
    startPlc1Comm();
}

void MainWindow::changeEvent(QEvent *e)
{
    if (e->type() == QEvent::LanguageChange) {
        ui->retranslateUi(this);
    }
    QMainWindow::changeEvent(e);
}

#include <QProcess>
void MainWindow::shutMeDown()
{
    if (!disable_shutdown) {
        //windows only
        QString program = "shutdown";
        QStringList arguments;
        arguments << "/s"
                  << "/t"
                  << "6";

        QProcess *myProcess = new QProcess();
        myProcess->start(program, arguments);
        myProcess->waitForStarted(2000);
    }
    QTimer::singleShot(1000, this, SLOT(close()));
}

void MainWindow::checkFullScreen()
{
#ifdef NOT_FOR_PRODUCTION
    return;
#endif
    if (!disable_shutdown) {
        this->show();
        this->raise();
        this->activateWindow();
        this->setWindowState(Qt::WindowNoState);
        this->setWindowState(Qt::WindowFullScreen);
    }
}

void MainWindow::periodic1000ms()
{
    qDebug() << "Periodic ...";
    //send PC is alive signal to PLC
    //plc1Proxy->writeData("H100:15",0x01); //PC alive
}

void MainWindow::mainTabChanged(int idx)
{
    if (idx == 1) {
        //
    } else {
        //
    }
}
