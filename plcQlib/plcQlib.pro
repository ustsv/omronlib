QT += core gui widgets network

TEMPLATE = lib
CONFIG += staticlib

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    cdoublespinbox.cpp \
    numeditdialog.cpp \
    omronfinscom.cpp \
    plcQlib.cpp \
    plcevent.cpp \
    plcevhandler.cpp \
    plcproxy.cpp \
    plcqlib.cpp \
    plcspinboxsender.cpp \
    plcwidgeteventhandler.cpp \
    plcwidgetsender.cpp \
    widgetplcinterface.cpp

HEADERS += \
    cdoublespinbox.h \
    numeditdialog.h \
    omronfinscom.h \
    plcevent.h \
    plcevhandler.h \
    plcproxy.h \
    plcqlib.h \
    plcspinboxsender.h \
    plcwidgeteventhandler.h \
    plcwidgetsender.h \
    types.h \
    widgetplcinterface.h

FORMS += \
    numeditdialog.ui

DISTFILES += \
    LICENSE

target.path += $$[QT_INSTALL_INCLUDES]/imageformats
target.files =

# Default rules for deployment.
!isEmpty(target.path): INSTALLS += target
