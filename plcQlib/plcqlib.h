#ifndef PLCQLIB_H
#define PLCQLIB_H


#include "types.h"
#include "plcproxy.h"
#include "widgetplcinterface.h"
#include "plcwidgetsender.h"
#include "plcevhandler.h"
#include "numeditdialog.h"
#include "plcspinboxsender.h"
#include "cdoublespinbox.h"
#include "plcwidgeteventhandler.h"

#endif // PLCQLIB_H
